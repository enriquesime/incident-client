import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtResponse } from '../models/jwt-response';
import {HOSTNAME} from '../shared/constants/server.constants';

const httpHeaders = {
  headers: new HttpHeaders({ 'Content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  host_name = HOSTNAME;
  private signinUrl = `http://${this.host_name}:8080/api/auth/signin`;
  private signupUrl = `http://${this.host_name}:8080/api/auth/signup`;

  constructor(private http: HttpClient) { }

   // JwtResponse(accessToken,type,username,authorities)
  attemptAuth(credentials: any): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.signinUrl, credentials, httpHeaders);
  }

   // SignUpInfo(name,username,email,role,password)
  signup(credentials: any): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.signupUrl, credentials, httpHeaders);
  }
}
