import {
    MatFormFieldModule, MatSelectModule, MatInputModule, MatCardModule, MatProgressSpinnerModule,
    MatButtonModule,
    // tslint:disable-next-line:max-line-length
    MatDatepickerModule, MatNativeDateModule, MatDividerModule, MatTableDataSource, MatTableModule, MatPaginatorModule, MatStepperModule, MatIconModule
} from '@angular/material';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    // tslint:disable-next-line:max-line-length
    imports: [MatFormFieldModule, MatSelectModule, MatInputModule, MatCardModule, MatProgressSpinnerModule, MatButtonModule, MatDatepickerModule, MatNativeDateModule, MatDividerModule, MatTableModule, CommonModule, MatPaginatorModule, MatStepperModule, MatIconModule],
    providers: [
        MatDatepickerModule,
    ],
    // tslint:disable-next-line:max-line-length
    exports: [MatFormFieldModule, MatSelectModule, MatInputModule, MatCardModule, MatProgressSpinnerModule, MatButtonModule, MatDatepickerModule, MatNativeDateModule, MatDividerModule, MatTableModule, CommonModule, MatPaginatorModule, MatStepperModule, MatIconModule]
})

export class MaterialModule {

}
