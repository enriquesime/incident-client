import { Employee, IEmployee } from './employee';
import { Client, IClient } from './client';





export interface IAddress {
    id?: number;
    street?: string;
    city?: string;
    state?: string;
    // zipCode?: string;
    // client?: IClient;
    // employee?: IEmployee;
}

export class Address implements IAddress {
    constructor(
        public id?: number,
        public street?: string,
        public city?: string,
        public state?: string
        // public client?: IClient,
        // public employee?: IEmployee
    ) {}
}

// export class Address {
//     private id: number;
//     private city: string;
//     private street: string;
//     private state: string;
//     private zipCode: string;
//     private employee: Employee;
//     private client: Client;


//     get _id(): number {
//         return this.id;
//     }
//     set _id(_id: number) {
//         this.id = _id;
//     }

//     get _city(): string {
//         return this.city;
//     }
//     set _city(_city: string) {
//         this.city = _city;
//     }

//     get _street(): string {
//         return this.street;
//     }
//     set _street(_street: string) {
//         this.street = _street;
//     }

//     get _state(): string {
//         return this.state;
//     }
//     set _state(_state: string) {
//         this.state = _state;
//     }

//     get _zipCode(): string {
//         return this.zipCode;
//     }
//     set _zipCode(_zipCode: string) {
//         this.zipCode = _zipCode;
//     }

//     get _employee(): Employee {
//         return this.employee;
//     }
//     set _employee(_employee: Employee) {
//         this.employee = _employee;
//     }

//     get _client(): Client {
//         return this.client;
//     }
//     set _client(_client: Client) {
//         this.client = _client;
//     }
// }
