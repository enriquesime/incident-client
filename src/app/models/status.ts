export interface IStatus {
    id?: number;
    name?: string;
    statusCode?: string;
}

export class Status implements IStatus {
    constructor(public id?: number, public name?: string, public statusCode?: string) { }
}

//     id: number;
//     name: string;
//     statusCode: string;
//     // incident: Incident;

//     get _id() {
//         return this.id;
//     }

//     set _id(_id: number) {
//        this.id = _id;
//     }

//     get _name() {
//         return this.name;
//     }

//     set _name(_name: string) {
//        this.name = _name;
//     }

//     get _statusCode() {
//         return this.statusCode;
//     }

//     set _statusCode(_statusCode: string) {
//        this.statusCode = _statusCode;
//     }

//     // get _incident() {
//     //     return this.incident;
//     // }

//     // set _incident(_incident: Incident) {
//     //    this.incident = _incident;
//     // }

// }
