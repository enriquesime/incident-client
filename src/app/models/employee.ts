import { IIncident } from './incident';
import { IAddress } from './address';
import { IPhone } from './phone';
import { IMaintenance } from './maintenance';
import { Moment } from 'moment';

export interface IEmployee {
    id?: number;
    name?: string;
    lastName?: string;
    email?: string;
    documentId?: string;
    hireDate?: Moment;
    birthDate?: Moment;
    salary?: number;
    photo?: number;
    incidents?: IIncident[];
    phones?: IPhone[];
    addresses?: IAddress[];
    maintenances?: IMaintenance[];
}

export class Employee implements IEmployee {
    constructor(
        public id?: number,
        public name?: string,
        public lastName?: string,
        public email?: string,
        public documentId?: string,
        public hireDate?: Moment,
        public birthDate?: Moment,
        public salary?: number,
        public photo?: number,
        public incidents?: IIncident[],
        public phones?: IPhone[],
        public addresses?: IAddress[],
        public maintenances?: IMaintenance[]
    ) {}
}


// export class Employee {
//     id: number;
//     name: string;
//     lastName: string;
//     documentId: string;
//     photo: string;
//     incident: Incident;
//     addresses: Address[];
//     phones: Phone[];
//     maintenances: Maintenance[];
// }

