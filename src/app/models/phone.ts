import { Employee, IEmployee } from './employee';
import { Client, IClient } from './client';


export interface IPhone {
    id?: number;
    type?: string;
    contactNumber?: string;
    // client?: IClient;
    // employee?: IEmployee;
}

export class Phone implements IPhone {
    // tslint:disable-next-line:max-line-length
    constructor(public id?: number, public type?: string, public contactNumber?: string) { }
}


// export class Phone {
//    private id: number;
//    private type: string;
//    private contactNumber: string;
//    private employee: Employee;
//    private client: Client;


//     get _id(): number {
//         return this.id;
//     }
//     set _id(_id: number) {
//         this.id = _id;
//     }

//     get _type(): string {
//         return this.type;
//     }
//     set _type(_type: string) {
//         this.type = _type;
//     }

//     get _contactNumber(): string {
//         return this.contactNumber;
//     }
//     set _contactNumber(_contactNumber: string) {
//         this.contactNumber = _contactNumber;
//     }

//     get _employee(): Employee {
//         return this.employee;
//     }
//     set _employee(_employee: Employee) {
//         this.employee = _employee;
//     }

//     get _client(): Client {
//         return this.client;
//     }
//     set _client(_client: Client) {
//         this.client = _client;
//     }
// }
