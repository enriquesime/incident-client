
import {  IEmployee } from './employee';
import {IClient } from './client';
import {  IStatus } from './status';
import * as moment from "moment";

export interface IIncident {
    id?: number;
    name?: string;
    issueDescription?: string;
    solutionDescription?: string;
    client?: IClient;
    status?: IStatus;
    employee?: IEmployee;
}

export class Incident implements IIncident {

  constructor(public id?: number,
        public name?: string,
        public solutionDescription?: string,
        public issueDescription?: string,
        public creationDate?: Date,
        public client?: IClient,
        public status?: IStatus,
        public employee?: IEmployee) {

  }


}




    // get _id(): number {
    //     return this.id;
    // }
    // set _id(_id: number) {
    //     this.id = _id;
    // }

    // get _name(): string {
    //     return this.name;
    // }
    // set _name(_name: string) {
    //     this.name = _name;
    // }

    // get _issueDescription(): string {
    //     return this.issueDescription;
    // }
    // set _issueDescription(_issueDescription: string) {
    //     this.issueDescription = _issueDescription;
    // }


    // get _solutionDescription(): string {
    //     return this.solutionDescription;
    // }
    // set _solutionDescription(_solutionDescription: string) {
    //     this.solutionDescription = _solutionDescription;
    // }

    // get _status(): Status {
    //     return this.status;
    // }
    // set _status(_status: Status) {
    //     this.status = _status;
    // }

    // get _client(): Client {
    //     return this.client;
    // }
    // set _client(_client: Client) {
    //     this.client = _client;
    // }

    // get _employee(): Employee {
    //     return this.employee;
    // }
    // set __employee(_employee: Employee) {
    //     this.employee = _employee;
    // }







