import { Address, IAddress } from './address';
import { Incident, IIncident } from './incident';
import { Phone, IPhone } from './phone';
import { IMaintenance } from './maintenance';



export interface IClient {
    id?: number;
    name?: string;
    email?: string;
    maintenances?: IMaintenance[];
    incidents?: IIncident[];
    phones?: IPhone[];
    addresses?: IAddress[];
}

export class Client implements IClient {
    constructor(
        public id?: number,
        public name?: string,
        public email?: string,
        public maintenances?: IMaintenance[],
        public incidents?: IIncident[],
        public phones?: IPhone[],
        public addresses?: IAddress[]
    ) { }
}
