import { Employee, IEmployee } from './employee';
import { Moment } from 'moment';
import { IClient } from './client';

export interface IMaintenance {
    id?: number;
    date?: Moment;
    observation?: string;
    client?: IClient;
    employee?: IEmployee;
}

export class Maintenance implements IMaintenance {
    constructor(
        public id?: number,
        public date?: Moment,
        public observation?: string,
        public client?: IClient,
        public employee?: IEmployee
    ) {}
}
