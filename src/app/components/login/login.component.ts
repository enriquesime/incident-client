import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../../services/appservice';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthLoginInfo } from 'src/app/models/auth-login-info';
import { AuthService } from 'src/app/auth/auth.service';
import { TokenService } from 'src/app/auth/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AppServiceService]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  private isLoggedIn = false;
  private isLoginFailed = false;
  private roles: string[] = [];
  private loginInfo: AuthLoginInfo;
  private errorMessage = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private tokenService: TokenService,
    private authService: AuthService
    // private alertService: AlertService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required,Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

    if (this.tokenService.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenService.getAuthorities();
    }



    // reset login status
    // this.tokenService.signOut();
   

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loginInfo = new AuthLoginInfo(this.loginForm.get('username').value, this.loginForm.get('password').value);

    this.loading = true;

    this.authService.attemptAuth(this.loginInfo)
      .subscribe(data => {
        this.tokenService.saveToken(data.accessToken);
        this.tokenService.saveUsername(data.username);
        this.tokenService.saveAuthorities(data.authorities);

        this.isLoggedIn = true;
        this.isLoginFailed = false;
        swal({
          title: 'User Loggued!',
          text: 'Logged In successfully!',
          icon: 'success',
        });
        this.router.navigate(['/home']).then(data =>{
          this.reloadPage();
        });

      }, error => {
        console.log(error);
        this.isLoggedIn = false;
        this.isLoginFailed = true;
        this.errorMessage = error.error.message;

        swal({
          title: 'Error to Login!',
          text: this.errorMessage,
          icon: 'error',
        }).then(function () {

          window.location.reload();
      
      });;
       
      });
    // this.authenticationService.login(this.f.username.value, this.f.password.value)
    //     .pipe(first())
    //     .subscribe(
    //         data => {
    //             this.router.navigate([this.returnUrl]);
    //         },
    //         error => {
    //             this.alertService.error(error);
    //             this.loading = false;
    //         });
  }

  reloadPage() {
    window.location.reload();
  }

}
