import {  Component,   OnInit,   Input,   ViewChild} from '@angular/core';
import { Incident } from '../../../models/incident';
import { IncidentService } from '../../../services/incident.service';
import swal from 'sweetalert2';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
@Component({
  selector: 'app-incident-list',
  templateUrl: './incident-list.component.html',
  styleUrls: ['./incident-list.component.css']
})
export class IncidentListComponent implements OnInit {
  dataSource: MatTableDataSource<Incident>;
  @Input() incidents: Incident[];
  incident: Incident;
  displayedColumns = ['id', 'name', 'issueDescription','creationDate', 'solutionDescription',  'client', 'employee', 'details'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private incidentService: IncidentService) { }

  ngOnInit() {
    this.getIncidents();
    console.log('incidents :' + this.incidents);
  }

  getIncidents() {
    this.incidentService.getIncidents()
      .subscribe(data => {
        this.incidents = data;
        console.log(JSON.stringify(data));
        console.log('array :' + JSON.stringify(this.incidents));
        this.dataSource = new MatTableDataSource(this.incidents);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  deleteIncident(id: number) {

    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.incidentService.deleteIncident(id)
          .subscribe(data => {
            this.getIncidents();
          });
        swal(
          'Deleted!',
          'Incident has been deleted.',
          'success'
        );
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  onSelectIncident(incident: Incident) {
    console.log('array :' + JSON.stringify(incident));
    this.incident = incident;
  }

}
