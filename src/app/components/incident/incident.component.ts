import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Incident, IIncident } from '../../models/incident';
import { IncidentService } from '../../services/incident.service';
import { IStatus } from '../../models/status';
import { IClient, Client } from '../../models/client';
import { Employee, IEmployee } from '../../models/employee';
import { StatusService } from '../../services/status.service';
import swal from 'sweetalert2';
import { EmployeeService } from '../../services/employee.service';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.css']
})
export class IncidentComponent implements OnInit {
  clientAndEmployeeFormGroup: FormGroup;
  incidentInfoFormGroup: FormGroup;
  statuses: IStatus[];
  status: IStatus;
  clients: IClient[];
  employees: Employee[];
  client: Client;
  employee: Employee;
  _incident: IIncident;
  constructor(private fb: FormBuilder, private router: Router, private incidentService: IncidentService,
    private statusService: StatusService, private clientService: ClientService, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.incidentInfoFormGroup = this.fb.group({
      name: ['', Validators.required],
      issueDescription: ['', Validators.required]
    });

    this.clientAndEmployeeFormGroup = this.fb.group({
      client: ['', Validators.required],
      employee: ['', Validators.required]
    });

    this.getIncidents();
    this.getClients();
    this.getEmployees();

  }

  createIncident() {
    if (this.incidentInfoFormGroup.valid && this.clientAndEmployeeFormGroup.valid) {

      // take client info
      // emp: Employee;
      // cli: Client;
      this._incident = new Incident();
      this._incident.name = this.incidentInfoFormGroup.get('name').value;
      this._incident.issueDescription = this.incidentInfoFormGroup.get('issueDescription').value;
      

      console.log("id :"+this.clientAndEmployeeFormGroup.get('employee').value.id);
      this.employee= new Employee();
      this.employee.id =this.clientAndEmployeeFormGroup.get('employee').value.id
      this.employee.name =this.clientAndEmployeeFormGroup.get('employee').value.name
      this.employee.lastName =this.clientAndEmployeeFormGroup.get('employee').value.lastName
      this.employee.email =this.clientAndEmployeeFormGroup.get('employee').value.email
      this.employee.documentId =this.clientAndEmployeeFormGroup.get('employee').value.documentId
      this.employee.hireDate =this.clientAndEmployeeFormGroup.get('employee').value.hireDate
      this.employee.birthDate =this.clientAndEmployeeFormGroup.get('employee').value.birthDate
      this.employee.salary =this.clientAndEmployeeFormGroup.get('employee').value.salary



      this.client= new Client();
      this.client.id =this.clientAndEmployeeFormGroup.get('client').value.id
      this.client.name =this.clientAndEmployeeFormGroup.get('client').value.name
      this.client.email =this.clientAndEmployeeFormGroup.get('client').value.email


      this._incident.employee=this.employee;
      this._incident.client=this.client;
      
      // emp.id=this.employee.id;
      // emp.name=this.employee.name;
      // emp.lastName=this.employee.lastName;
      // emp.email=this.employee.email;
      // emp.documentId=this.employee.documentId;
      // emp.hireDate=this.employee.hireDate;
      // emp.birthDate=this.employee.birthDate;
      // emp.salary=this.employee.salary;

      // cli = new Client();
      // cli.name=this.client.name;
      // cli.id=this.client.id;
      // cli.email=this.employee.email;

      // this._address.city = this.addressFormGroup.get('city').value;
      // this._address.state = this.addressFormGroup.get('state').value;
      // this._address.street = this.addressFormGroup.get('street').value;
      // this._address.zipCode = this.addressFormGroup.get('zipCode').value;


      // this._phone = new Phone();
      // this._phone.contactNumber = this.phoneFormGroup.get('contactNumber').value;
      // this._phone.type = this.phoneFormGroup.get('typ').value;

      
      

      // this._incident.client = this.client;
      // this._incident.employee = this.employee;

      //  const incidents = [this._incident];
      //  this.client.incidents = incidents;
      // this.employee.incidents = incidents;
      // // const phones = [this._phone];
      // this._employee.addresses = addresses;
      // this._employee.phones = phones;

    //   this.client.incidents = [this._incident];
    // this.employee.incidents = [this._incident];
      // this._incident.client = cli;
      // this._incident.employee = emp;





      // console.log('client :' + this.clientInfoFormGroup.get('name').value);
      // console.log('client :' + this.clientInfoFormGroup.get('email').value);

      // console.log('client :' + this.addressFormGroup.get('city').value);
      // console.log('client :' + this.addressFormGroup.get('state').value);
      // console.log('client :' + this.addressFormGroup.get('street').value);
      // console.log('client :' + this.addressFormGroup.get('zipCode').value);
      // console.log('client :' + this.phoneFormGroup.get('number').value);
      // console.log('client :' + this.phoneFormGroup.get('tipo').value);

      console.log('employee :' + JSON.stringify(this.employee));
      console.log('client  :' + JSON.stringify(this.client));
      console.log('incident :' + JSON.stringify(this._incident));
      // console.log('phone1 :' + JSON.stringify(this._phone));
      // console.log('client address [] :' + JSON.stringify(this._employee.addresses));

      // // // take address info



      this.incidentService.createIncident(this._incident)
        .subscribe(data => {
          this.router.navigate(['incident-list']);
          swal({
            position: 'top',
            type: 'success',
            title: 'Incident has been added',
            showConfirmButton: false,
            timer: 1500
          });
        });
    } else {

      // swal({
      //   type: 'error',
      //   title: 'Oops...',
      //   text: 'All fields are',
      //   footer: '<a href>Why do I have this issue?</a>'
      // });
    }

    //   this.incidentService.createIncident(incident)
    //     .subscribe(data => {
    //       this.router.navigate(['/incident-list']);
    //       swal({
    //         title: 'Incident Created!',
    //         text: 'The Incident Was Created Succesfully!',
    //         icon: 'success',
    //       });
    //     });
    // }

  }

  getIncidents() {

    this.incidentService.getIncidents().
      subscribe(data => {
        console.log('data : ' + JSON.stringify(data));
      });
  }


  getClients() {

    this.clientService.getClients().
      subscribe(data => {
        console.log('data : ' + JSON.stringify(data));
        this.clients =  data;
      });
  }

  getEmployees() {

    this.employeeService.getEmployees().
      subscribe(data => {
        console.log('data : ' + JSON.stringify(data));
        this.employees = data;
      });
  }
  goBack() {
    this.router.navigate(['/home']);
  }


}
