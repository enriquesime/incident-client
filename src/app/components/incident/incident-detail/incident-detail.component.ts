import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Incident } from '../../../models/incident';
import { Router, ActivatedRoute } from '@angular/router';
import { IncidentService } from '../../../services/incident.service';
import { Location } from '@angular/common';
import swal from 'sweetalert2';
@Component({
  selector: 'app-incident-details',
  templateUrl: './incident-detail.component.html',
  styleUrls: ['./incident-detail.component.css']
})
export class IncidentDetailComponent implements OnInit {

  detailForm: FormGroup;
  @Input() incident: Incident;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private router: Router, private incidentService: IncidentService, private activatedRoute: ActivatedRoute
    , private location: Location) { }

  ngOnInit() {
    this.getIncident();
    this.detailForm = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      documentId: ['', Validators.required]
    });
  }

  // onSubmit() {
  //   this.incidentService.createEmployee(this.detailForm.value)
  //     .subscribe(data => {
  //       alert('User created');
  //       this.router.navigate(['employee-list']);
  //     });
  // }

  updateIncident(incident: Incident) {
    this.incidentService.updateIncident(incident)
      .subscribe(data => {
        swal({
          position: 'top',
          type: 'success',
          title: 'Incident has been updated',
          showConfirmButton: false,
          timer: 1500
        });
      });

  }


  getIncident() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.incidentService.getIncident(id)
      .subscribe(incident => {
        this.incident = incident;
        console.log(' incident details ' + JSON.stringify(incident));
      });
  }


  goBack() {
    this.location.back();
  }

}
