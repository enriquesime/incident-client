import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Incident, IIncident } from '../../../models/incident';
import { Router, ActivatedRoute } from '@angular/router';
import { IncidentService } from '../../../services/incident.service';
import { Location } from '@angular/common';
import swal from 'sweetalert2';
import { IClient } from '../../../models/client';
import { IStatus } from '../../../models/status';
import { IEmployee } from '../../../models/employee';
import { ClientService } from '../../../services/client.service';
import { StatusService } from '../../../services/status.service';
import { EmployeeService } from '../../../services/employee.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-incident-update',
  templateUrl: './incident-update.component.html',
  styleUrls: ['./incident-update.component.css']
})
export class IncidentUpdateComponent implements OnInit {
  private _incident: IIncident;
  isSaving: boolean;

  clients: IClient[];

  statuses: IStatus[];

  employees: IEmployee[];

  detailForm: FormGroup;
  @Input() incident: Incident;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private router: Router, private incidentService: IncidentService,
    private clientService: ClientService,
    private statusService: StatusService,
    private employeeService: EmployeeService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    // this.getIncident();
    this.detailForm = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      documentId: ['', Validators.required]
    });
    console.log('ngOnInit .....');

    this.activatedRoute.data.subscribe(({ incident }) => {
      this.incident = incident;

      console.log('activatedRoute loaded' + JSON.stringify(incident));
    });

    this.clientService.query().subscribe(
      (res: HttpResponse<IClient[]>) => {
        this.clients = res.body;
        console.log('clientService loaded' + JSON.stringify(res));
      }
    );

    this.employeeService.query().subscribe(
      (res: HttpResponse<IEmployee[]>) => {
        this.employees = res.body;
        console.log('employeeService loaded' + JSON.stringify(res));
      }
    );

    this.statusService.query().subscribe(
      (res: HttpResponse<IStatus[]>) => {
        this.statuses = res.body;
        console.log('statusService loaded' + JSON.stringify(res));
      }
    );



  }

  // onSubmit() {
  //   this.incidentService.createEmployee(this.detailForm.value)
  //     .subscribe(data => {
  //       alert('User created');
  //       this.router.navigate(['employee-list']);
  //     });
  // }

  updateIncident(incident: Incident) {
    this.incidentService.updateIncident(incident)
      .subscribe(data => {
        swal({
          position: 'top',
          type: 'success',
          title: 'Incident has been updated',
          showConfirmButton: false,
          timer: 1500
        });
      });

  }


  getIncident() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.incidentService.getIncident(id)
      .subscribe(incident => {
        this.incident = incident;
        console.log(' incident details ' + JSON.stringify(incident));
      });
  }


  // goBack() {
  //   this.location.back();
  // }

}
