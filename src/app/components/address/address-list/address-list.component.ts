
import { Component, OnInit, Input, AfterViewInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Address } from '../../../models/address';
import { AddressService } from '../../../services/address.service';

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.css']
})
export class AddressListComponent implements OnInit {

  @Input() addresses: Address[];
  displayedColumns = ['id', 'city', 'state', 'zipCode', 'employee', 'client', 'delete'];
  dataSource: MatTableDataSource<Address>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private addressService: AddressService) { }

  ngOnInit() {
    this.getAddresses();
    console.log('employees :' + this.addresses);
  }


  getAddresses() {
    this.addressService.getAddresses()
      .subscribe(data => {
        this.addresses = data;
        console.log(JSON.stringify(data));
        console.log('array :' + JSON.stringify(this.addresses));
        this.dataSource = new MatTableDataSource(this.addresses);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });

  }


  deleteAddress(id: number) {
    this.addressService.deleteAddress(id)
      .subscribe(data => {
        console.log('address Deleted ' + id);

      });

  }


  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

}
