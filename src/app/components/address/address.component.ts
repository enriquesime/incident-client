import { Component, OnInit } from '@angular/core';
import { AddressService } from '../../services/address.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private addresService: AddressService) { }

  addForm: FormGroup;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      // zipCode: ['', Validators.required]
    });
  }

  onSubmit() {
    this.addresService.createAddress( this.addForm.value )
      .subscribe(data => {
        this.router.navigate(['address-list']);
        // swal({
        //   position: 'top',
        //   type: 'success',
        //   title: `Cliente creado con éxito`,
        //   showConfirmButton: false,
        //   timer: 1500
        // });
      });
  }

}
