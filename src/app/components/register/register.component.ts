import { Component, OnInit } from '@angular/core';
import { AuthSignupInfo } from 'src/app/models/auth-signup-info';
import { AuthService } from 'src/app/auth/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private signupInfo: AuthSignupInfo;
  private isSignedUp = false;
  private isSignUpFailed = false;
  private errorMessage = '';
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  constructor(private formBuilder: FormBuilder,
    private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30)]],
      username: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    console.log(this.registerForm);
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }


       this.signupInfo = new AuthSignupInfo(this.registerForm.get('name').value, this.registerForm.get('username').value,
       this.registerForm.get('email').value, this.registerForm.get('password').value);
       this.loading = true;

    console.log('this.signupInfo :' + JSON.stringify(this.signupInfo));
    this.authService.signup(this.signupInfo)
      .subscribe(data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;
        swal({
          title: 'User Registered!',
          text: 'Registration successful!',
          icon: 'success',
        });

        this.router.navigate(['/auth/signin']);
      },
        error => {
          console.log(error);
          this.isSignedUp = false;
          this.isSignUpFailed = true;
          this.errorMessage = error.error.message;
     });
  }
}
