import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../../../services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Employee } from '../../../models/employee';
import { Location } from '@angular/common';
import swal from 'sweetalert2';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  updateForm: FormGroup;
  @Input() employee: Employee;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private router: Router, private employeeService: EmployeeService, private activatedRoute: ActivatedRoute
    , private location: Location) { }

  ngOnInit() {
    this.getEmployee();
    this.updateForm = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      documentId: ['', Validators.required]
    });
  }

  onSubmit() {
    this.employeeService.createEmployee(this.updateForm.value)
      .subscribe(data => {
        alert('User created');
        this.router.navigate(['employee-list']);
      });
  }

  updateEmployee(employee: Employee) {
    this.employeeService.updateEmployee(employee)
      .subscribe(data => {
        swal({
          position: 'top',
          type: 'success',
          title: 'Employee has been updated',
          showConfirmButton: false,
          timer: 1500
        });
      });

  }


  getEmployee() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.employeeService.getEmployee(id)
      .subscribe(employee => {
        this.employee = employee;
        console.log(' employee details ' + JSON.stringify(employee));
      });
  }


  goBack() {
    this.location.back();
  }
}
