import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Employee } from '../../../models/employee';
import { EmployeeService } from '../../../services/employee.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import swal from 'sweetalert2';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  @Input() employees: Employee[];
  employee: Employee;
  displayedColumns = ['id', 'name', 'lastName', 'documentId', 'details', 'delete'];
  dataSource: MatTableDataSource<Employee>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.getEmployees();
    // console.log('employees :' + this.employees);
  }


  getEmployees() {

    this.employeeService.getEmployees()
      .subscribe(data => {
        this.employees = data;
        // console.log(JSON.stringify(data));
        // console.log('array :' + JSON.stringify(this.employees));
        this.dataSource = new MatTableDataSource(this.employees);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  deleteEmployee(id: number) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.employeeService.deleteEmployee(id)
          .subscribe(data => {
            this.getEmployees();
          });
        swal(
          'Deleted!',
          'Employee has been deleted.',
          'success'
        );
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  onSelectEmployee(employee: Employee) {
    console.log('array :' + JSON.stringify(employee));
    this.employee = employee;
  }
}
