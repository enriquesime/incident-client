import { Component, OnInit, } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {  IEmployee, Employee } from '../../models/employee';
import { Location } from '@angular/common';
import swal from 'sweetalert2';
import { IPhone, Phone } from '../../models/phone';
import { IAddress, Address } from '../../models/address';
import { IncidentService } from '../../services/incident.service';
import { IIncident } from '../../models/incident';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  providers: [EmployeeService],
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  phoneFormGroup: FormGroup;
  employeeInfoFormGroup: FormGroup;
  addressFormGroup: FormGroup;
  _employee: IEmployee;
  _phone: IPhone;
  _address: IAddress;
  incidents: IIncident[];

  constructor(private fb: FormBuilder, private router: Router, private location: Location, private employeeService: EmployeeService, private incidentService: IncidentService) { }

  ngOnInit() {
    console.log('EmployeeComponent init ');
    // this.addForm = this.fb.group({
    //   name: ['', Validators.required],
    //   lastName: ['', Validators.required],
    //   email: ['', Validators.required],
    //   documentId: ['', Validators.required]
    // });

    this.employeeInfoFormGroup = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      documentId: ['', Validators.required]
    });


    this.addressFormGroup = this.fb.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      // zipCode: ['', Validators.required]
    });

    this.phoneFormGroup = this.fb.group({
      typ: ['', Validators.required],
      contactNumber: ['', Validators.required]
    });
  }



  createEmployee() {




    if (this.employeeInfoFormGroup.valid && this.addressFormGroup.valid && this.phoneFormGroup.valid) {

      // take client info

      this._employee = new Employee();
      this._employee.name = this.employeeInfoFormGroup.get('name').value;
      this._employee.lastName = this.employeeInfoFormGroup.get('lastName').value;
      this._employee.email = this.employeeInfoFormGroup.get('email').value;
      this._employee.documentId = this.employeeInfoFormGroup.get('documentId').value;

      this._address = new Address();
      this._address.city = this.addressFormGroup.get('city').value;
      this._address.state = this.addressFormGroup.get('state').value;
      this._address.street = this.addressFormGroup.get('street').value;
      // this._address.zipCode = this.addressFormGroup.get('zipCode').value;


      this._phone = new Phone();
      this._phone.contactNumber = this.phoneFormGroup.get('contactNumber').value;
      this._phone.type = this.phoneFormGroup.get('typ').value;

      // this._address.client = this._client;
      // this._phone.client = this._client;

      const addresses = [this._address];
      const phones = [this._phone];
      this._employee.addresses = addresses;
      this._employee.phones = phones;

      // this.client.addresses = [this.address];
      // this.client.phones = [this.phone];





      // console.log('client :' + this.clientInfoFormGroup.get('name').value);
      // console.log('client :' + this.clientInfoFormGroup.get('email').value);

      // console.log('client :' + this.addressFormGroup.get('city').value);
      // console.log('client :' + this.addressFormGroup.get('state').value);
      // console.log('client :' + this.addressFormGroup.get('street').value);
      // console.log('client :' + this.addressFormGroup.get('zipCode').value);
      // console.log('client :' + this.phoneFormGroup.get('number').value);
      // console.log('client :' + this.phoneFormGroup.get('tipo').value);

      console.log('client1 :' + JSON.stringify(this._employee));
      console.log('phone1 :' + JSON.stringify(this._phone));
      console.log('client address [] :' + JSON.stringify(this._employee.addresses));

      // // take address info



      this.employeeService.createEmployee(this._employee)
        .subscribe(data => {
          this.router.navigate(['employee-list']);
          swal({
            position: 'top',
            type: 'success',
            title: 'Employee has been added',
            showConfirmButton: false,
            timer: 1500
          });
        });
    } else {

      // swal({
      //   type: 'error',
      //   title: 'Oops...',
      //   text: 'All fields are',
      //   footer: '<a href>Why do I have this issue?</a>'
      // });
    }

  }

  getIncidents() {
    // this.incidentService.getIncidents()
    //   .subscribe(data => {
    //     console.log('client incidents [] :' + JSON.stringify(data));
    //    this.incidents = data;

    //   });
  }

  goBack() {
    this.router.navigate(['/home']);
  }
  // onDestroy() {
  //   this.employeeService.unsubscribe();
  // }
}
