import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Client } from '../../../models/client';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientService } from '../../../services/client.service';
import { Location } from '@angular/common';
import swal from 'sweetalert2';
@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.css']
})
export class ClientDetailComponent implements OnInit {
  updateForm: FormGroup;
  @Input() client: Client;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private router: Router, private clientService: ClientService, private activatedRoute: ActivatedRoute
    , private location: Location) { }

  ngOnInit() {
    this.getClient();
    this.updateForm = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  updateClient(client: Client) {
    this.clientService.updateClient(client)
      .subscribe(data => {
        swal({
          position: 'top',
          type: 'success',
          title: 'Client has been updated',
          showConfirmButton: false,
          timer: 1500
        });
      });

  }


  getClient() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.clientService.getClient(id)
      .subscribe(client => {
        this.client = client;
        console.log(' client details ' + JSON.stringify(client));
      });
  }


  goBack() {
    this.location.back();
  }
}
