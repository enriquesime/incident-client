import { IAddress, Address } from '../../models/address';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ClientService } from '../../services/client.service';
import swal from 'sweetalert';
import { Client } from '../../models/client';
import { Phone } from '../../models/phone';
import { AddressService } from '../../services/address.service';
import { PhoneService } from '../../services/phone.service';
import { IncidentService } from '../../services/incident.service';
import { IIncident } from '../../models/incident';


@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  phoneFormGroup: FormGroup;
  clientInfoFormGroup: FormGroup;
  addressFormGroup: FormGroup;
  _client: Client;
  _phone: Phone;
  _address: Address;
  incidents: IIncident[];
  addForm: FormGroup;



  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private router: Router, private clientService: ClientService, private addressService: AddressService, private phoneService: PhoneService, private incidentService: IncidentService) { }

  ngOnInit() {

    // this.addForm = this.fb.group({
    //   name: ['', Validators.required],
    //   email: ['', Validators.required]

    // });

    this.clientInfoFormGroup = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
    });
    this.phoneFormGroup = this.fb.group({
      typ: ['', Validators.required],
      contactNumber: ['', Validators.required]
    });
    // this.secondFormGroup = this.fb.group({
    //   secondCtrl: ['', Validators.required]
    // });

    this.addressFormGroup = this.fb.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required]
    });

    // this.thirdFormGroup = this.fb.group({
    //   // street: ['', Validators.required],
    //  city: ['', Validators.required]
    //   // state: ['', Validators.required],
    //   // zipCode: ['', Validators.required]
    // });
  }


  createClient() {
    if (this.clientInfoFormGroup.valid && this.addressFormGroup.valid && this.phoneFormGroup.valid) {
      // take client info

      this._client = new Client();
      this._client.name = this.clientInfoFormGroup.get('name').value;
      this._client.email = this.clientInfoFormGroup.get('email').value;

      this._address = new Address();
      this._address.city = this.addressFormGroup.get('city').value;
      this._address.state = this.addressFormGroup.get('state').value;
      this._address.street = this.addressFormGroup.get('street').value;


      this._phone = new Phone();
      this._phone.contactNumber = this.phoneFormGroup.get('contactNumber').value;
      this._phone.type = this.phoneFormGroup.get('typ').value;

      // this._address.client = this._client;
      // this._phone.client = this._client;

      const addresses = [this._address];
      const phones = [this._phone];
      this._client.addresses = addresses;
      this._client.phones = phones;

      // this.client.addresses = [this.address];
      // this.client.phones = [this.phone];





      // console.log('client :' + this.clientInfoFormGroup.get('name').value);
      // console.log('client :' + this.clientInfoFormGroup.get('email').value);

      // console.log('client :' + this.addressFormGroup.get('city').value);
      // console.log('client :' + this.addressFormGroup.get('state').value);
      // console.log('client :' + this.addressFormGroup.get('street').value);
      // console.log('client :' + this.addressFormGroup.get('zipCode').value);
      // console.log('client :' + this.phoneFormGroup.get('number').value);
      // console.log('client :' + this.phoneFormGroup.get('tipo').value);

      console.log('client1 :' + JSON.stringify(this._client));
      console.log('phone1 :' + JSON.stringify(this._phone));
      console.log('client address [] :' + JSON.stringify(this._client.addresses));

      // // take address info


      // // take phone info

      this.clientService.createClient(this._client)
        .subscribe(data => {
          this.router.navigate(['client-list']);
          swal({
            title: 'Client Created!',
            text: 'The Client Was Created Succesfully!',
            icon: 'success',
          });
        },
          error => {
            console.log(error);
          });

    }

  }

  getIncidents() {
    // this.incidentService.getIncidents()
    //   .subscribe(data => {
    //     console.log('client incidents [] :' + JSON.stringify(data));
    //    this.incidents = data;

    //   });
  }

  goBack() {
    this.router.navigate(['/home']);
  }

}
