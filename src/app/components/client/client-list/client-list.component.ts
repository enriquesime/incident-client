
import { Component, OnInit, Input, AfterViewInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ClientService } from '../../../services/client.service';
import { Client } from '../../../models/client';
import swal from 'sweetalert2';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  @Input() clients: Client[];
  client: Client;
  displayedColumns = ['id', 'name', 'email', 'details', 'delete'];
  dataSource: MatTableDataSource<Client>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private clientService: ClientService) { }

  ngOnInit() {
    this.getClients();
    // console.log('clients :' + this.clients);
  }


  getClients() {
    this.clientService.getClients()
      .subscribe(data => {
        this.clients = data;
        // console.log(JSON.stringify(data));
        // console.log('array :' + JSON.stringify(this.clients));
        this.dataSource = new MatTableDataSource(this.clients);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      });
    // .pipe(
    //   map(response => this.employees=response)

    // );


  }

  deleteClient(id: number) {

    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.clientService.deletetClient(id)
          .subscribe(data => {
            // console.log('client Deleted ' + client);
            this.getClients();

          },
          error => {
            console.log(error);
          });
        swal(
          'Deleted!',
          'Employee has been deleted.',
          'success'
        );
      }
    });




  }

  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  onSelectClient(client: Client) {
    console.log('array :' + JSON.stringify(client));
    this.client = client;
  }

}
