import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { StatusService } from '../../../services/status.service';
import { Status } from '../../../models/status';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-status-list',
  templateUrl: './status-list.component.html',
  styleUrls: ['./status-list.component.css']
})
export class StatusListComponent implements OnInit {

  @Input() statuses: Status[];
  status: Status;
  displayedColumns = ['id', 'name', 'statusCode'];
  dataSource: MatTableDataSource<Status>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private statusService: StatusService) { }

  ngOnInit() {
    this.getStatuses();
    console.log('statuses :' + this.statuses);
  }


  getStatuses() {
    this.statusService.getStatuses()
      .subscribe(data => {
        this.statuses = data;
        console.log(JSON.stringify(data));
        console.log('array :' + JSON.stringify(this.statuses));
        this.dataSource = new MatTableDataSource(this.statuses);
        this.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }


}
