
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  addForm: FormGroup;
  user: User;

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService) { }

  ngOnInit() {
    console.log('UserComponent init ');
    this.addForm = this.fb.group({
      userName: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }



  createUser() {

    if (this.addForm.valid) {
      this.userService.createUser(this.addForm.value)
        .subscribe(data => {
          this.router.navigate(['employee-list']);
          swal({
            position: 'top',
            type: 'success',
            title: 'Employee has been added',
            showConfirmButton: false,
            timer: 1500
          });
        });
    } else {

      // swal({
      //   type: 'error',
      //   title: 'Oops...',
      //   text: 'All fields are',
      //   footer: '<a href>Why do I have this issue?</a>'
      // });
    }

  }

  goBack() {
    this.router.navigate(['/home']);
  }
  // onDestroy() {
  //   this.employeeService.unsubscribe();
  // }
}
