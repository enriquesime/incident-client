import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { User } from '../../../models/user.model';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { UserService } from '../../../services/user.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @Input() users: User[];
  user: User;
  displayedColumns = ['id', 'userName', 'password', 'firstName', 'lastName', 'update' ];
  dataSource: MatTableDataSource<User>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
    // console.log('employees :' + this.employees);
  }


  getUsers() {

    this.userService.getUsers()
      .subscribe(data => {
        this.users = data;
        // console.log(JSON.stringify(data));
        // console.log('array :' + JSON.stringify(this.employees));
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  deleteEmployee(id: number) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.userService.deleteUser(id)
          .subscribe(data => {
            this.getUsers();
          });
        swal(
          'Deleted!',
          'Employee has been deleted.',
          'success'
        );
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
//   onSelectEmployee(employee: Employee) {
//     console.log('array :' + JSON.stringify(employee));
//     this.employee = employee;
//   }
// }

}
