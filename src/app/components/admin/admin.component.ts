import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/loginService';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  board: string;
  errorMessage = '';
  
  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.loginService.getAdminBoard()
     .subscribe(data => {
       console.log(data);
       this.board = data;
     }, error => {

      this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
     });
  }

}
