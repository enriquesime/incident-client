import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Phone } from '../../../models/phone';
import { PhoneService } from '../../../services/phone.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-phone-list',
  templateUrl: './phone-list.component.html',
  styleUrls: ['./phone-list.component.css']
})
export class PhoneListComponent implements OnInit {

  @Input() phones: Phone[];
  displayedColumns = ['id', 'type', 'description', 'update'];
  dataSource: MatTableDataSource<Phone>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private phoneService: PhoneService) { }

  ngOnInit() {
    this.getEmployees();
    console.log('phones :' + this.phones);
  }


  getEmployees() {
    this.phoneService.getPhones()
      .subscribe(data => {
        this.phones = data;
        console.log(JSON.stringify(data));
        console.log('array :' + JSON.stringify(this.phones));
        this.dataSource = new MatTableDataSource(this.phones);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;

      });
  }

  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }


}
