import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MaintenanceService } from '../../services/maintenance.service';
import { Maintenance } from '../../models/maintenance';
import swal from 'sweetalert';
import { IEmployee, Employee } from '../../models/employee';
import { IClient, Client } from '../../models/client';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { IStatus } from '../../models/status';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  providers: [MaintenanceService],
  styleUrls: ['./maintenance.component.css']
})
export class MaintenanceComponent implements OnInit {
  maintenanceInfoFormGroup: FormGroup;
  clientAndEmployeeFormGroup: FormGroup;
  maintenance: Maintenance;
  clients: IClient[];
  employees: IEmployee[];
  client: IClient;
  employee: IEmployee;
  status: IStatus;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private router: Router, private maintenanceService: MaintenanceService, private clientService: ClientService, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.maintenanceInfoFormGroup = this.fb.group({
      date: ['', Validators.required],
      observation: ['', Validators.required]
    });

    this.clientAndEmployeeFormGroup = this.fb.group({
      client: ['', Validators.required],
      employee: ['', Validators.required]
    });

    this.getClients();
    this.getEmployees();
  }

 

  // onSubmit() {
  //   this.maintenanceService.createMaintenance(this.addForm.value)
  //     .subscribe(data => {

  //     });
  // }

  createMaintenance() {

    if (this.maintenanceInfoFormGroup.valid && this.clientAndEmployeeFormGroup.valid) {


      this.maintenance = new Maintenance();
      this.maintenance.date = this.maintenanceInfoFormGroup.get('date').value;
      this.maintenance.observation = this.maintenanceInfoFormGroup.get('observation').value;

      console.log("id :" + this.clientAndEmployeeFormGroup.get('employee').value.id);
      this.employee = new Employee();
      this.employee.id = this.clientAndEmployeeFormGroup.get('employee').value.id;
      this.employee.name = this.clientAndEmployeeFormGroup.get('employee').value.name;
      this.employee.lastName = this.clientAndEmployeeFormGroup.get('employee').value.lastName;
      this.employee.email = this.clientAndEmployeeFormGroup.get('employee').value.email;
      this.employee.documentId = this.clientAndEmployeeFormGroup.get('employee').value.documentId;
      this.employee.hireDate = this.clientAndEmployeeFormGroup.get('employee').value.hireDate;
      this.employee.birthDate = this.clientAndEmployeeFormGroup.get('employee').value.birthDate;
      this.employee.salary = this.clientAndEmployeeFormGroup.get('employee').value.salary;

      this.client = new Client();
      this.client.id = this.clientAndEmployeeFormGroup.get('client').value.id;
      this.client.name = this.clientAndEmployeeFormGroup.get('client').value.name;
      this.client.email = this.clientAndEmployeeFormGroup.get('client').value.email;

      this.maintenance.employee = this.employee;
      this.maintenance.client = this.client;

      console.log('form: ' + JSON.stringify(this.maintenance));
      this.maintenanceService.createMaintenance(this.maintenance)
        .subscribe(data => {

          this.router.navigate(['/maintenance-list']);
          swal({
            title: 'Maintenance Created!',
            text: 'The Maintenance Was Created Succesfully!',
            icon: 'success',
          });
        });
    }

  }


  getClients() {

    this.clientService.getClients().
      subscribe(data => {
        console.log('data : ' + JSON.stringify(data));
        this.clients =  data;
      });
  }

  getEmployees() {

    this.employeeService.getEmployees().
      subscribe(data => {
        console.log('data : ' + JSON.stringify(data));
        this.employees = data;
      });
  }
  goBack() {
    this.router.navigate(['/home']);
  }

}
