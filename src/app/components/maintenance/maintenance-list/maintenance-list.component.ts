
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MaintenanceService } from '../../../services/maintenance.service';
import { Maintenance } from '../../../models/maintenance';
import swal from 'sweetalert2';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
@Component({
  selector: 'app-maintenance-list',
  templateUrl: './maintenance-list.component.html',
  styleUrls: ['./maintenance-list.component.css']
})
export class MaintenanceListComponent implements OnInit {

  @Input() maintenances: Maintenance[];
  maintenance: Maintenance;
  displayedColumns = ['id', 'date', 'observation', 'client', 'employee', 'delete','details'];
  dataSource: MatTableDataSource<Maintenance>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private maintenanceService: MaintenanceService) { }

  ngOnInit() {
    this.getMaintenances();
    console.log('maintenances :' + this.maintenance);
  }

  getMaintenances() {
    this.maintenanceService.getMaintenances()
      .subscribe(data => {
        this.maintenances = data;
        console.log(JSON.stringify(data));
        this.dataSource = new MatTableDataSource(this.maintenances);
        this.dataSource.data = this.maintenances;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  deleteMaintenance(id: number) {

    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.maintenanceService.deleteMaintenance(id)
          .subscribe(data => {
            console.log('maintenance Deleted ');
            this.getMaintenances();
          });
        swal(
          'Deleted!',
          'Maintenance has been deleted.',
          'success'
        );
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue.trim();
    filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

}
