import { Component, OnInit, Input } from '@angular/core';
import { Maintenance } from '../../../models/maintenance';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MaintenanceService } from '../../../services/maintenance.service';
import { Location } from '@angular/common';
import swal from 'sweetalert2';
@Component({
  selector: 'app-maintenance-detail',
  templateUrl: './maintenance-detail.component.html',
  styleUrls: ['./maintenance-detail.component.css']
})
export class MaintenanceDetailComponent implements OnInit {

  detailForm: FormGroup;
  @Input() maintenance: Maintenance;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private router: Router, private maintenanceService: MaintenanceService, private activatedRoute: ActivatedRoute
    , private location: Location) { }

  ngOnInit() {
    this.getMaintenance();
    this.detailForm = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      documentId: ['', Validators.required]
    });
  }

  updateMaintenance(maintenance: Maintenance) {
    this.maintenanceService.updateMaintenance(maintenance)
      .subscribe(data => {
        swal({
          position: 'top',
          type: 'success',
          title: 'Maintenance has been updated',
          showConfirmButton: false,
          timer: 1500
        });
      });
  }

  getMaintenance() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.maintenanceService.getMaintenance(id)
      .subscribe(maintenance => {
        this.maintenance = maintenance;
        console.log(' maintenance details ' + JSON.stringify(maintenance));
      });
  }


  goBack() {
    this.location.back();
  }

}
