import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './components/client/client.component';
import { ClientDetailComponent } from './components/client/client-detail/client-detail.component';
import { AddressComponent } from './components/address/address.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { IncidentComponent } from './components/incident/incident.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { PhoneComponent } from './components/phone/phone.component';
import { StatusComponent } from './components/status/status.component';
import { EmployeeListComponent } from './components/employee/employee-list/employee-list.component';
import { AddressListComponent } from './components/address/address-list/address-list.component';
import { ClientListComponent } from './components/client/client-list/client-list.component';
import { IncidentListComponent } from './components/incident/incident-list/incident-list.component';
import { MaintenanceListComponent } from './components/maintenance/maintenance-list/maintenance-list.component';
import { PhoneListComponent } from './components/phone/phone-list/phone-list.component';
import { StatusListComponent } from './components/status/status-list/status-list.component';
import { EmployeeDetailComponent } from './components/employee/employee-detail/employee-detail.component';
import { HomeComponent } from './components/home/home.component';
import { IncidentDetailComponent } from './components/incident/incident-detail/incident-detail.component';
import { MaintenanceDetailComponent } from './components/maintenance/maintenance-detail/maintenance-detail.component';
import { UserComponent } from './components/user/user.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { IncidentUpdateComponent } from './components/incident/incident-update/incident-update.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { RegisterComponent } from './components/register/register.component';
import { ExampleComponent } from './components/example/example.component';
import {AuthGuardService} from './services/auth-guard.service';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home', component: HomeComponent
  },
  { path: 'clients', component: ClientComponent , canActivate: [AuthGuardService]},
  {
    path: 'addresses', component: AddressComponent , canActivate: [AuthGuardService]
    // , children: [
    //   // { path: '', redirectTo: 'address-list', pathMatch: 'full' },

    // ]
  },
  { path: 'users', component: UserComponent , canActivate: [AuthGuardService] },
  { path: 'user-list', component: UserListComponent , canActivate: [AuthGuardService] },
  { path: 'address-list', component: AddressListComponent , canActivate: [AuthGuardService] },
  { path: 'client-detail/:id', component: ClientDetailComponent , canActivate: [AuthGuardService] },
  { path: 'employee-detail/:id', component: EmployeeDetailComponent , canActivate: [AuthGuardService] },
  { path: 'incident-detail/:id', component: IncidentDetailComponent  , canActivate: [AuthGuardService]},
  { path: 'maintenance-detil/:id', component: MaintenanceDetailComponent , canActivate: [AuthGuardService]},
  { path: 'employee-list', component: EmployeeListComponent },
  {
    path: 'employees', component: EmployeeComponent , canActivate: [AuthGuardService]

  },

  { path: 'incidents', component: IncidentComponent , canActivate: [AuthGuardService] },
  { path: 'incidents-update', component: IncidentUpdateComponent , canActivate: [AuthGuardService]},
  { path: 'maintenances', component: MaintenanceComponent , canActivate: [AuthGuardService] },
  { path: 'phones', component: PhoneComponent  , canActivate: [AuthGuardService]},
  { path: 'statuses', component: StatusComponent  , canActivate: [AuthGuardService]},
  { path: 'client-list', component: ClientListComponent , canActivate: [AuthGuardService] },
  { path: 'maintenance-list', component: MaintenanceListComponent , canActivate: [AuthGuardService] },
  { path: 'incident-list', component: IncidentListComponent , canActivate: [AuthGuardService]},
  { path: 'phone-list', component: PhoneListComponent , canActivate: [AuthGuardService]},
  { path: 'status-list', component: StatusListComponent , canActivate: [AuthGuardService]},
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'auth/signin',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: RegisterComponent
  },
  {
    path: 'example',
    component: ExampleComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
