import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule  } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './/app-routing.module';
import { ClientComponent } from './components/client/client.component';
import { ClientListComponent } from './components/client/client-list/client-list.component';
import { MaterialModule } from './material';
import { AddressComponent } from './components/address/address.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { IncidentComponent } from './components/incident/incident.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { StatusComponent } from './components/status/status.component';
import { PhoneComponent } from './components/phone/phone.component';
import { EmployeeListComponent } from './components/employee/employee-list/employee-list.component';
import { AddressListComponent } from './components/address/address-list/address-list.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { IncidentListComponent } from './components/incident/incident-list/incident-list.component';
import { MaintenanceListComponent } from './components/maintenance/maintenance-list/maintenance-list.component';
import { PhoneListComponent } from './components/phone/phone-list/phone-list.component';
import { StatusListComponent } from './components/status/status-list/status-list.component';
import { EmployeeDetailComponent } from './components/employee/employee-detail/employee-detail.component';
import { HomeComponent } from './components/home/home.component';
import { IncidentDetailComponent } from './components/incident/incident-detail/incident-detail.component';
import { ClientDetailComponent } from './components/client/client-detail/client-detail.component';
import { MaintenanceDetailComponent } from './components/maintenance/maintenance-detail/maintenance-detail.component';
import { LoginComponent } from './components/login/login.component';
import { UserComponent } from './components/user/user.component';
import { UserService } from './services/user.service';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { UserDetailComponent } from './components/user/user-detail/user-detail.component';
import { IncidentUpdateComponent } from './components/incident/incident-update/incident-update.component';
import { StatusService } from './services/status.service';
import { EmployeeService } from './services/employee.service';
import { ClientService } from './services/client.service';
import { AdminComponent } from './components/admin/admin.component';
import { RegisterComponent } from './components/register/register.component';
import { TextMaskModule } from 'angular2-text-mask';
import { ExampleComponent } from './components/example/example.component';
import { httpInterceptorProviders } from './auth/auth-interceptor';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
@NgModule({
  declarations: [
    AppComponent,
    ClientComponent,
    ClientListComponent,
    AddressComponent,
    EmployeeComponent,
    IncidentComponent,
    MaintenanceComponent,
    StatusComponent,
    PhoneComponent,
    EmployeeListComponent,
    AddressListComponent,
    IncidentListComponent,
    MaintenanceListComponent,
    PhoneListComponent,
    StatusListComponent,
    EmployeeDetailComponent,
    HomeComponent,
    IncidentDetailComponent,
    ClientDetailComponent,
    MaintenanceDetailComponent,
    LoginComponent,
    UserComponent,
    UserListComponent,
    UserDetailComponent,
    IncidentUpdateComponent,
    AdminComponent,
    RegisterComponent,
    ExampleComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TextMaskModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],

  providers: [httpInterceptorProviders,UserService, StatusService, EmployeeService, ClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
