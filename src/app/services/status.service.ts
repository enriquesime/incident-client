import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Status, IStatus } from '../models/status';
import { createRequestOption } from '../shared/request.util';
import {HOSTNAME} from '../shared/constants/server.constants';

type EntityArrayResponseType = HttpResponse<IStatus[]>;

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/statuses`;
  httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {
    console.log('Servicio Status Funcionando');
  }

  getStatuses(): Observable<Status[]> {
    // return this.http.get(this.baseUrl).pipe(
    //   map(data => data as Address[])
    // );
    return this.http.get<Status[]>(this.baseUrl);

  }

  getStatus(id: number): Observable<Status> {

    return this.http.get<Status>(`${this.baseUrl}/${id}`);
  }

  createStatuses(status: Status): Observable<Status> {
    return this.http.post<Status>(this.baseUrl, status, { headers: this.httpHeaders });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IStatus[]>(this.baseUrl, { params: options, observe: 'response' });
  }

  updateStatus(status: Status): Observable<Status> {
    return this.http.put<Status>(this.baseUrl, status, { headers: this.httpHeaders });
  }

  deleteStatus(id: number): Observable<Status> {
    return this.http.put<Status>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
