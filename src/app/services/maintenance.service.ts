import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Maintenance } from '../models/maintenance';
import {HOSTNAME} from '../shared/constants/server.constants';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceService {
  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/maintenances`;
  httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {
    console.log('Servicio Maintenance Funcionando');
  }

  getMaintenances(): Observable<Maintenance[]> {
    // return this.http.get(this.baseUrl).pipe(
    //   map(data => data as Address[])
    // );
    return this.http.get<Maintenance[]>(this.baseUrl);

  }

  getMaintenance(id: number): Observable<Maintenance> {
    return this.http.get<Maintenance>(`${this.baseUrl}/${id}`);
  }

  createMaintenance(maintenance: Maintenance): Observable<Maintenance> {
    return this.http.post<Maintenance>(this.baseUrl, maintenance, { headers: this.httpHeaders });
  }

  updateMaintenance(maintenance: Maintenance): Observable<Maintenance> {
    return this.http.put<Maintenance>(this.baseUrl, maintenance, { headers: this.httpHeaders });
  }

  deleteMaintenance(id: number): Observable<Maintenance> {
    return this.http.delete<Maintenance>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
