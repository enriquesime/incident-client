import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee, IEmployee } from '../models/employee';
import { createRequestOption } from '../shared/request.util';
import {HOSTNAME} from '../shared/constants/server.constants';
type EntityArrayResponseType = HttpResponse<IEmployee[]>;
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/employees`;
  httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) {
    console.log('Servicio Employee Funcionando');
  }

  getEmployees(): Observable<Employee[]> {
    // return this.http.get(this.baseUrl).pipe(
    //   map(data => data as Address[])
    // );
    return this.http.get<Employee[]>(this.baseUrl);

  }

  getEmployee(id: number): Observable<Employee> {
    return this.http.get<Employee>(`${this.baseUrl}/${id}`);
  }

  createEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.baseUrl, employee, { headers: this.httpHeaders });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmployee[]>(this.baseUrl, { params: options, observe: 'response' });
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    return this.http.put<Employee>(this.baseUrl, employee, { headers: this.httpHeaders });
  }

  deleteEmployee(id: number): Observable<Employee> {
    return this.http.delete<Employee>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
