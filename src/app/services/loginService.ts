import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {HOSTNAME} from '../shared/constants/server.constants';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  host_name = HOSTNAME;
  public baseUserUrl = `http://${this.host_name}:8080/api/test/user`;
  public basePmUrl = `http://${this.host_name}:8080/api/test/pm`;
  public baseAdminUrl = `http://${this.host_name}:8080/api/test/admin`;

  constructor(private http: HttpClient) { }

  getUserBoard(): Observable<string> {
    return this.http.get(this.baseUserUrl, { responseType: 'text' });
  }

  getPmBoard(): Observable<string> {
    return this.http.get(this.basePmUrl, { responseType: 'text' });
  }

  getAdminBoard(): Observable<string> {
    return this.http.get(this.baseAdminUrl, { responseType: 'text' });
  }
}
