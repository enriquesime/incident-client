import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Client, IClient } from '../models/client';
import { createRequestOption } from '../shared/request.util';
import {HOSTNAME} from '../shared/constants/server.constants';

type EntityResponseType = HttpResponse<IClient>;
type EntityArrayResponseType = HttpResponse<IClient[]>;

@Injectable({ providedIn: 'root' })
export class ClientService {
  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/clients`;
  httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' });

  constructor(private http: HttpClient) {
    console.log('Client Service Working...');
  }

  getClients(): Observable<Client[]> {
    return this.http.get(this.baseUrl).pipe(
      map(data => data as Client[])
    );
  }

  getClient(id: number): Observable<Client> {
    return this.http.get<Client>(`${this.baseUrl}/${id}`);
  }
  createClient(client: Client): Observable<Client> {
    // console.log('service client :' + JSON.stringify(client));
    return this.http.post<Client>(this.baseUrl, client, { headers: this.httpHeaders });
  }
  updateClient(client: Client): Observable<Client> {
    return this.http.put<Client>(this.baseUrl, client, { headers: this.httpHeaders });

  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Client[]>(this.baseUrl, { params: options, observe: 'response' });
  }

  deletetClient(id: number): Observable<Client> {

    return this.http.delete<Client>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
