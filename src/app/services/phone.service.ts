import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Phone } from '../models/phone';
import {HOSTNAME} from '../shared/constants/server.constants';
@Injectable({
  providedIn: 'root'
})
export class PhoneService {
  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/phones`;
  httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {
    console.log('Servicio Phone Funcionando');
  }

  getPhones(): Observable<Phone[]> {
    // return this.http.get(this.baseUrl).pipe(
    //   map(data => data as Address[])
    // );
    return this.http.get<Phone[]>(this.baseUrl);

  }

  getPhone(id: number): Observable<Phone> {
    return this.http.get<Phone>(`${this.baseUrl}/${id}`);
  }

  createPhone(phone: Phone): Observable<Phone> {
    return this.http.post<Phone>(this.baseUrl, phone, { headers: this.httpHeaders });
  }

  updatePhone(phone: Phone): Observable<Phone> {
    return this.http.put<Phone>(this.baseUrl, phone, { headers: this.httpHeaders });
  }

  deletePhone(id: number): Observable<Phone> {
    return this.http.put<Phone>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
