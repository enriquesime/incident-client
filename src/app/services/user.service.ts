import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import {HOSTNAME} from '../shared/constants/server.constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/users`;
  constructor(private http: HttpClient) { }

  // getUsers(): Observable<User> {
  //   return this.http.get<User>(this.baseUrl + '/users');
  // }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl, { headers: this.httpHeaders });
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl, user, { headers: this.httpHeaders });
  }

  deleteUser(id: number): Observable<User> {
    return this.http.put<User>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
