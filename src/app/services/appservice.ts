import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import {HOSTNAME} from '../shared/constants/server.constants';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {
  host_name = HOSTNAME;
  private baseUrl = `http://${this.host_name}:8080/`;

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User> {

    return this.http.get<User>(this.baseUrl + '/users');
  }
}
