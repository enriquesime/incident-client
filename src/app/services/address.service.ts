import { Injectable } from '@angular/core';
import { Address } from '../models/address';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {HOSTNAME} from '../shared/constants/server.constants';




@Injectable({
  providedIn: 'root'
})
export class AddressService {
  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/addresses`;
  httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {
    console.log('Servicio Address Funcionando');
  }

  getAddresses(): Observable<Address[]> {
    // return this.http.get(this.baseUrl).pipe(
    //   map(data => data as Address[])
    // );
    return this.http.get<Address[]>(this.baseUrl);

  }

  getAddress(id: number): Observable<Address> {
    return this.http.get<Address>(`${this.baseUrl}/${id}`);
  }

  createAddress(address: Address): Observable<Address> {
    return this.http.post<Address>(this.baseUrl, address, { headers: this.httpHeaders });
  }

  updateAddress(address: Address): Observable<Address> {
    return this.http.put<Address>(this.baseUrl, address, { headers: this.httpHeaders });
  }

  deleteAddress(id: number): Observable<Address> {
    return this.http.delete<Address>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
