import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Incident, IIncident } from '../models/incident';
import {HOSTNAME} from '../shared/constants/server.constants';

@Injectable({
  providedIn: 'root'
})
export class IncidentService {

  host_name = HOSTNAME;
  baseUrl = `http://${this.host_name}:8080/api/incidents`;
  httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {
    console.log('Servicio Incident Funcionando');
  }

  getIncidents(): Observable<Incident[]> {
    // return this.http.get(this.baseUrl).pipe(
    //   map(data => data as Address[])
    // );
    return this.http.get<IIncident[]>(this.baseUrl);

  }

  getIncident(id: number): Observable<Incident> {
    return this.http.get<Incident>(`${this.baseUrl}/${id}`);
  }

  createIncident(incident: IIncident): Observable<Incident> {
    return this.http.post<Incident>(this.baseUrl, incident, { headers: this.httpHeaders });
  }

  updateIncident(incident: Incident): Observable<Incident> {
    return this.http.put<Incident>(this.baseUrl, incident, { headers: this.httpHeaders });
  }

  deleteIncident(id: number): Observable<Incident> {
    return this.http.delete<Incident>(`${this.baseUrl}/${id}`, { headers: this.httpHeaders });
  }
}
