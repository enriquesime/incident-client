import { Injectable } from '@angular/core';
import {RouterStateSnapshot} from '@angular/router';
import {TokenService} from '../auth/token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private tokenService: TokenService) { }

  canActivate(_route, state: RouterStateSnapshot) {
    const  token = this.tokenService.getToken();
      if (token !== null) {
        return true;
      }
      // this.router.navigate(['/login'], {
      //   queryParams: { returnUrl: state.url }
      // });
      return false;

    }
}
